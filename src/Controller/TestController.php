<?php
/**
 * @file
 * Contains \Drupal\chartjs\Controller\TestController.
 */

namespace Drupal\chartjs\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class TestController.
 *
 * @package Drupal\chartjs\Controller
 */
class TestController extends ControllerBase {

  /**
   * Test markup page.
   *
   * @return array
   *   The response as rendered array.
   */
  public function content() {
    return array(
      '#theme' => 'chartjs',
      '#element_id' => 'test-canvas',
      '#width' => 200,
      '#height' => 200,
      '#type' => 'line',
      '#labels' => ["January", "February", "March", "April", "May", "June", "July"],
      '#datasets' => [
        [
          "label" => "My First dataset 1",
          "backgroundColor" => "rgba(255,99,132,0.2)",
          "borderColor" => "rgba(255,99,132,1)",
          "borderWidth" => 1,
          "hoverBackgroundColor" => "rgba(255,99,132,0.4)",
          "hoverBorderColor" => "rgba(255,99,132,1)",
          "data" => [65, 59, 80, 81, 56, 55, 40],
        ],
        [
          "label" => "My First dataset 2",
          "backgroundColor" => "rgba(255,99,132,0.2)",
          "borderColor" => "rgba(255,99,132,1)",
          "borderWidth" => 1,
          "hoverBackgroundColor" => "rgba(255,99,132,0.4)",
          "hoverBorderColor" => "rgba(255,99,132,1)",
          "data" => [20, 10, 30, 81, 56, 65, 90],
        ],
      ],
    );
  }
}