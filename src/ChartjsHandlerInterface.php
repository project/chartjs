<?php

namespace Drupal\chartjs;

/**
 * Chart.js plugin interface.
 */
interface ChartjsHandlerInterface {

  /**
   * Method returns the supported chart types.
   *
   * @return array
   *   The supported chart types defined by each Plugin.
   */
  public function supportedTypes();

  /**
   * Renders a chart with the given parameters from the theme function.
   *
   * @param string $id
   *   Element's id attribute.
   *   This will be used to convert the element to a chart.js chart.
   * @param array $data
   *   The label and the data-set for creating a chart.
   * @param array $options
   *   Extra options for the chart.js JavaScript.
   *
   * @return array
   *   Renderer array which will be converted to HTML by Drupal itself.
   */
  public function render($id, array $data, array $options = []);

}
