<?php

namespace Drupal\chartjs\Plugin\chartjs\handler;

use Drupal\chartjs\ChartjsHandlerInterface;

/**
 * Chart.js Line chart plugin.
 *
 * @Plugin(
 *   id = "chartjs_line",
 *   name = "chartjs_line",
 *   label = @Translation("Line Chart")
 * )
 */
class LineChartHandler implements ChartjsHandlerInterface {

  /**
   * The machine name of the chart.
   *
   * @var string
   */
  public $name = 'linechart';

  /**
   * {@inheritdoc}
   */
  public function supportedTypes() {
    return ['line'];
  }

  /**
   * {@inheritdoc}
   */
  public function render($id, array $data, array $options = []) {
    if (empty($data)) {
      throw new \InvalidArgumentException("Data can't be empty.");
    }
    $chart['#attached'] = [
      'drupalSettings' => [
        'chartjsDataSettings' => [
          $id => [
            'type' => $data['type'],
            'data' => [
              'labels' => $data['labels'],
              'datasets' => $data['datasets'],
            ],
            'options' => $options,
          ],
        ],
      ],
      'library' => [
        'chartjs/chartjs.cdn',
        'chartjs/chartjs.init',
      ],
    ];

    return $chart;
  }

}
