<?php

namespace Drupal\chartjs\Plugin;

use Drupal\Component\Plugin\PluginManagerBase;
use Drupal\Component\Plugin\Discovery\DerivativeDiscoveryDecorator;
use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Plugin\Discovery\AnnotatedClassDiscovery;

/**
 * Plugin type manager for all views handlers.
 */
class ChartjsHandlerManager extends PluginManagerBase {

  /**
   * Constructs a ViewsHandlerManager object.
   *
   * @param string $type
   *   The plugin type, for example line/bar etc.
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   */
  public function __construct($type, \Traversable $namespaces) {
    $this->discovery = new AnnotatedClassDiscovery("Plugin/chartjs/$type", $namespaces);
    $this->discovery = new DerivativeDiscoveryDecorator($this->discovery);
    $this->factory = new DefaultFactory($this->discovery);
  }

}
