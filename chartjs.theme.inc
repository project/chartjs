<?php
/**
 * @file
 * Contains the theme callback function for Chart.js module.
 */

use Drupal\Core\Template\Attribute;

/**
 * Pre-process method used for hook theme.
 *
 * @param array $vars
 *   The variables for the theme.
 */
function template_preprocess_chartjs(&$vars) {
  $element_id = isset($vars['element_id']) ? $vars['element_id'] : 'chartjs_' . uniqid();
  $type = $vars['type'];
  $width = isset($vars['width']) ? $vars['width'] : '400';
  $height = isset($vars['height']) ? $vars['height'] : '400';
  $data = [
    'type' => $vars['type'],
    'labels' => $vars['labels'],
    'datasets' => $vars['datasets'],
  ];
  $options = $vars['options'];
  // Get the plugin.
  $plugin = chartjs_plugin_discovery($type);
  $vars['chart'] = $plugin->render($element_id, $data, $options);
  $vars['chart_attributes'] = new Attribute([
    'id' => $element_id,
    'class' => [
      'chartjs-chart',
      'chartjs-chart-' . $plugin->name,
    ],
    'width' => $width,
    'height' => $height,
  ]);
}
