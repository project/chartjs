/**
 * @file
 * Chart.js initialization JavaScript.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.chartjs = {
    attach: function (context) {
      var $chartInstance = $(context).find('.chartjs-chart').once('chartjs-processed');
      if ($chartInstance.length) {
        $chartInstance.each(function () {
          var elementId = this.id;
          var chartMeta = drupalSettings.chartjsDataSettings[elementId];
          // Create a new chart display.
          new Chart(this.getContext('2d'), {
            type: chartMeta.type,
            data: chartMeta.data,
            options: chartMeta.options
          });
        });
      }
    }
  };
})(jQuery, Drupal, drupalSettings);
